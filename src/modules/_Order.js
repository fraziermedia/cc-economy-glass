CustomOrder.Order = React.createClass({

	componentDidMount: function () {
	},

	getInitialState: function () {
		return {
			minDimension: 1,
			maxDimension: 100,
			width: undefined,
			height: undefined,
			strength: "Annealed",
			thickness: 4,
			tint: undefined,
			textured: 0,
			backpainted: false,
			glass: undefined,
			glassName: undefined,
			edgework: undefined,
			fabrications: [],
			currentStep: 0
		}
	},

	getThicknessLabel: function (thickness) {
		return _.findWhere(this.props.data.thickness, { Thickness: thickness.toString() }).Label
	},

	getGlassOptions: function () {
		console.log("Filtering glass options")
		var self = this
		// Find matching glasses
		var options = _.map(_.filter(self.props.data.glass, function (option) {
			var found = true
			// Is the glass strength available for this brand?
			if (option[self.state.strength] == "0") { found = false }
			// Is this thickness available?
			if (option.Thickness !== self.state.thickness.toString()) { found = false }
			// Does the glass come textured or not?
			if (option.Textured !== self.state.textured.toString()) { found = false }
			return found
		}), function (option) {
			option.Label = self.getThicknessLabel(option.Thickness)
			option.Price = Number(option[self.state.strength]).toFixed(2)
			return option
		})
		return options
	},

	getEdgeworkOptions: function () {
		console.log("Filtering edgework options")
		var self = this
		return _.filter(self.props.data.edgework, function (option) {
			var found = true
			// Is this thickness available?
			if (option.Thickness !== self.state.thickness.toString()) { found = false }
			return found
		})
	},

	getFabricationOptions: function () {
		console.log("Filtering fabrication options")
		var self = this
		return _.filter(self.props.data.fabrication, function (option) {
			var found = true
			// Is this thickness available?
			if (option.Thickness !== self.state.thickness.toString()) { found = false }
			return found
		})
	},

	handleInput: function (event) {
		var self = this
		var regexNumber = /^[\d.]+$/,
			state = {},
			key = event.target.name,
			value = event.target.value

		// Validate data
		switch (key) {
			case "width":
				if (regexNumber.test(value)) {
					value = parseFloat(value)
					if (this.state.strength === "Annealed") {
						if (value < this.state.minDimension) {  value = this.state.minDimension }
						if (value > this.state.glass.MaxWidthA) {
							value = this.state.glass.MaxWidthA
							window.alert("Please select a value equal to or less than " + this.state.glass.MaxWidthA)
						}
					} else {
						if (value < this.state.minDimension) {  value = this.state.minDimension }
						if (value > this.state.glass.MaxWidthT) {
							value = this.state.glass.MaxWidthT
							window.alert("Please select a value equal to or less than " + this.state.glass.MaxWidthT)
						}
					}
				}
				break
			case "height":
				if (regexNumber.test(value)) {
					value = parseFloat(value)
					if (this.state.strength === "Annealed") {
						if (value < this.state.minDimension) {  value = this.state.minDimension }
						if (value > this.state.glass.MaxHeightA) {
							value = this.state.glass.MaxHeightA
							window.alert("Please select a value less than " + this.state.glass.MaxHeightA)
						}
					} else {
						if (value < this.state.minDimension) {  value = this.state.minDimension }
						if (value > this.state.glass.MaxHeightT) {  
							value = this.state.glass.MaxHeightT
							window.alert("Please select a value less than " + this.state.glass.MaxHeightT)
						}
					}
				}
				break
			case "thickness":
				value = parseInt(value)
				state.edgework = undefined
				state.fabrications = []
				break
			case "textured":
				value = (value == "No") ? 0 : 1
				break
			case "strength":
				state.thickness = parseFloat(_.find(self.props.data.glass, function (glass) {
					return (glass.Name == self.state.glassName && glass[value] !== "0")
				}).Thickness)
				break
		}
		state[key] = value
		this.setState(state, self.setGlass)
	},

	setGlass: function () {
		var self = this,
			glass = _.findWhere(self.props.data.glass, {
				Name: self.state.glassName,
				Thickness: self.state.thickness.toString()
			})
		glass.Label = self.getThicknessLabel(glass.Thickness)
		glass.Price = Number(glass[self.state.strength]).toFixed(2)
		if (parseFloat(glass.Price) === 0) {
			// Glass not available
			console.log("Glass not available: " + glass.Name)
			this.setState({
				glass: undefined,
				glassName: undefined,
				edgeword: undefined,
				fabrications: []
			}, self.updateOrder)
		} else {
			console.log("Setting glass to: " + glass.Name)
			this.setState({
				glass: glass
			}, self.updateOrder)
		}
	},

	setGlassName: function (event) {
		var self = this,
			glassName = event.target.value
		this.setState({
			glassName: glassName,
			edgework: undefined,
			thickness: parseFloat(_.find(self.props.data.glass, function (glass) {
				return (glass.Name == glassName && glass[self.state.strength] !== "0")
			}).Thickness)
		}, self.setGlass)
	},

	setEdgework: function (event) {
		event.preventDefault()
		var self = this,
			edgework = JSON.parse(event.target.value)
		this.setState({
			edgework: edgework
		}, self.updateOrder)
	},

	addFabrication: function (fabrication, event) {
		event.preventDefault()
		var self = this,
			fabrications = this.state.fabrications
		fabrications.push(fabrication)
		this.setState({
			fabrications: fabrications
		}, self.updateOrder)
	},

	removeFabrication: function (index, event) {
		event.preventDefault()
		var self = this
			fabrications = this.state.fabrications
		fabrications.splice(index, 1)
		this.setState({
			fabrications: fabrications
		}, self.updateOrder)
	},

	setNextStep: function (event) {
		event.preventDefault()
		var self = this,
			currentStep = self.state.currentStep + 1,
			edgeworkOptions = self.getEdgeworkOptions(),
			fabricationOptions = self.getFabricationOptions()

		// Skip edgework or fabrication if they're not available for glass
		if (currentStep == 2 && edgeworkOptions.length == 1) { currentStep = currentStep + 1 }
		if (currentStep == 3 && fabricationOptions.length === 0) { currentStep = currentStep + 1 }

		if (this.state.currentStep === 0 && this.state.glassName === undefined) {
			alert("Please select a glass type before proceeding.")
			return
		}

		if (this.state.currentStep === 1 && (this.state.width === undefined || this.state.height === undefined)) {
			alert("Please set your width and height before proceeding.")
			return
		}

		// If we're on the 4th step, submit the form
		if (currentStep == 5) { self.props.completeOrder() }

		console.log("Changing to next step: " + currentStep)
		this.setState({
			currentStep: currentStep
		}, self.updateOrder)
	},

	setPreviousStep: function (event) {
		var self = this,
			currentStep = self.state.currentStep - 1,
			edgeworkOptions = self.getEdgeworkOptions(),
			fabricationOptions = self.getFabricationOptions()

		event.preventDefault()

		// Skip edgework or fabrication if they're not available for glass
		if (currentStep == 3 && fabricationOptions.length === 0) { currentStep = currentStep - 1 }
		if (currentStep == 2 && edgeworkOptions.length == 1) { currentStep = currentStep - 1 }

		console.log("Changing to previous step")

		this.setState({
			currentStep: currentStep
		}, self.updateOrder)
	},

	updateOrder: function () {
		var self = this,
			price = 0,
			edgework = 0,
			fabrications = 0,
			total = 0,
			orderDetails = ""

		// Make sure a glass type / brand has been selected!
		if (self.state.glass !== undefined && self.state.width !== undefined && self.state.height !== undefined) {

			var glass = self.state.glass
			var width = isNaN(parseFloat(self.state.width)) ? 0 : parseFloat(self.state.width)
			var height = isNaN(parseFloat(self.state.height)) ? 0 : parseFloat(self.state.height)

			orderDetails = 	glass.Name + "\n" +
								"  Strength: " + self.state.strength + "\n" +
								"  Textured: " + ((glass.Textured == "0") ? "No" : "Yes") + "\n" + 
								"  Thickness: " + glass.Label + "\n" +
								"  Dimensions: " + width + "\" (w) x " + height + "\" (h)\n\n"

			// Get the size in square inches
			var sizeSquareInch = width * height
			if (sizeSquareInch < glass.SqrMinTmp * 144) { sizeSquareInch = parseFloat(glass.SqrMinTmp) * 144 }

			var markup = (self.state.strength == "Annealed") ? glass.MarkupA : glass.MarkupT

			// Get the base price
			price = ((sizeSquareInch / 144) * glass.Price) * markup

			// Add edgework pricing
			if (self.state.edgework !== undefined) {
				edgework = ((width + height) * 2) * parseFloat(self.state.edgework.PricePerInch)
				orderDetails +=	"Edgework: " + self.state.edgework.Name + "\n\n"
			}

			// Add fabrication pricing
			if (self.state.fabrications.length > 0) {
				orderDetails +=	"Fabrications: \n"
				fabrications = _.reduce(self.state.fabrications, function (memo, fabrication) {
					memo = memo + Number(fabrication.Price)
					orderDetails += "- " + fabrication.Name + "\n"
					return memo
				}, 0)
			}

		} else {

			orderDetails = "Please continue with the ordering wizard for accurate pricing."

		}

		// Add it up... add it up...
		total = price + edgework + fabrications

		console.log("Order total: " + total)
		console.log(orderDetails)
		this.props.updateOrder( total, 1, orderDetails )
	},

	render: function () {
		var self = this

		var availableGlass = _.filter(self.props.data.glass, function (glass) {
			return parseFloat(glass[self.state.strength]) > 0
		})

		var glassOptions = _.uniq(_.pluck(availableGlass, "Name"))

		var thicknessOptions = _.map(_.pluck(_.where(availableGlass, { Name: self.state.glassName }), "Thickness"), function (thickness, index) {
			var thicknessLabel = _.findWhere(self.props.data.thickness, { Thickness: thickness })
			return (
				<option key={"Thickness"+index} value={thicknessLabel.Thickness}>{thicknessLabel.Label}</option>
			)
		})

		var textured = (this.state.textured === 0) ? false : true
		var edgeworkOptions = self.getEdgeworkOptions()
		var fabricationOptions = self.getFabricationOptions()

		var fabrications
		if (this.state.fabrications.length === 0) {
			fabrications = (
				<div className="instructions">None selected</div>
			)
		} else {
			fabrications = (
				<div className="selection-list">
					{_.map(self.state.fabrications, function (fabrication, index) {
						return (
							<div key={"Fabrication" + index} className="fabrication-selection mw-row option">
								<div className="label column-medium-6">{fabrication.Name}</div>
								<div className="control column-medium-6"><button className="fabrication-button" onClick={self.removeFabrication.bind(null, index)}>Remove</button></div>
							</div>
						)
					})}
				</div>
			)
		}

		var steps = [
					<CustomOrder.Step
						key="StepMaterial"
						className="Material"
						label="Select Glass Material"
						next={<button className="primary button" onClick={this.setNextStep}>Next Step</button>}>
						<table>
							<tbody>
								<tr>
									<th>Strength</th>
									<td className="radio-group">
										{_.map(["Annealed", "Tempered"], function (option) {
											var checked = (self.state.strength == option)
											return (
												<label key={"Strength" + option}><input name="strength" type="radio" value={option} checked={checked} onChange={self.handleInput} /> {option}</label>
											)
										})}
									</td>
								</tr>
								<tr>
									<th>Available Glass</th>
									<td>
										<select size="8" className="glass-options selection-list" value={self.state.glassName} onChange={self.setGlassName}>
											{_.map(glassOptions, function (glassName, index) {
												return (
													<option key={"Glass" + index} value={glassName}>
														{glassName}
													</option>
												)
											})}
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</CustomOrder.Step>,

					<CustomOrder.Step
						key="StepDimensions"
						className="Dimensions"
						label="Select Pane Dimensions"
						previous={<button className="secondary button" onClick={this.setPreviousStep}>Previous</button>}
						next={<button className="primary button" onClick={this.setNextStep}>Next Step</button>}>
						<table>
							<tbody>
								<tr>
									<th>Width (inches)</th>
									<td><input name="width" placeholder="Width in inches" type="number" step="0.1" value={this.state.width} onChange={this.handleInput} /></td>
								</tr>
								<tr>
									<th>Height (inches)</th>
									<td><input name="height" placeholder="Height in inches" type="number" step="0.1" value={this.state.height} onChange={this.handleInput} /></td>
								</tr>
								<tr>
									<th>Thickness</th>
									<td>
										<select name="thickness" value={this.state.thickness.toString()} onChange={this.handleInput}>
											{thicknessOptions}
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</CustomOrder.Step>,

					<CustomOrder.Step
						key="StepEdgework"
						className="Edgework"
						label="Select Edgework"
						previous={<button className="secondary button" onClick={this.setPreviousStep}>Previous</button>}
						next={<button className="primary button" onClick={this.setNextStep}>Next Step</button>}>
						<select size="8" className="edgework-options selection-list" value={JSON.stringify(self.state.edgework)} onChange={self.setEdgework}>
							{_.map(edgeworkOptions, function (option, index) {
								return (
									<option key={"Edgework" + index} value={JSON.stringify(option)}>
										{option.Name}
									</option>
								)
							})}
						</select>
					</CustomOrder.Step>,

					<CustomOrder.Step
						key="StepFabrication"
						className="Fabrication"
						label="Select Fabrication Options"
						previous={<button className="secondary button" onClick={this.setPreviousStep}>Previous</button>}
						next={<button className="primary button" onClick={this.setNextStep}>Next Step</button>}>

						<div className="mw-row">
							<div className="instructions">All fabrications are measured to the center of the hole, not the edge.</div>
							<div className="fabrication-options column-medium-6">
								<h3>Available Fabrication Options</h3>
								<div className="selection-list">
									{_.map(fabricationOptions, function (option, index) {
										return (
											<div key={"FabricationOption" + index} className="fabrication-option option" onClick={self.addFabrication.bind(null, option)}>{option.Name}</div>
										)
									})}
								</div>
							</div>
							<div className="fabrication-selections column-medium-6">
								<h3>Current Fabrication Selections</h3>
								{fabrications}
							</div>
						</div>
					</CustomOrder.Step>,

					<CustomOrder.Step
						key="StepUpload"
						className="Upload"
						label="Upload Special Instructions"
						previous={<button className="secondary button" onClick={this.setPreviousStep}>Previous</button>}
						next={<button className="primary button" onClick={this.props.completeOrder}>Complete Order</button>}>

						<p>If you have a drawing of your glass design or would like to provide additional instructions, please upload those materials here.</p>
						<div className="upload-row">
							<button className="button upload" onClick={self.props.uploadFile}>Select Uploads</button>
						</div>

					</CustomOrder.Step>
		]

		return (
			<div className="Order column-medium-9">
				<ReactCSSTransitionGroup transitionName="step" className="Steps" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
					{steps[this.state.currentStep]}
				</ReactCSSTransitionGroup>
			</div>
		)
	}
})

CustomOrder.Step = React.createClass({

	componentDidMount: function () {
	},

	getInitialState: function () {
		return {
		}
	},

	render: function () {
		var self = this

		var classes = [
			"Step",
			self.props.className
		].join(" ")

		return (
			<div className={classes}>
				<header>
					<h2>{this.props.label}</h2>
					<p>{this.props.instructions}</p>
				</header>
				<main>{this.props.children}</main>
				<footer className="controls mw-row">
					<div className="previous column-medium-6 column-small-6">{this.props.previous}&nbsp;</div>
					<div className="next column-medium-6 column-small-6">&nbsp;{this.props.next}</div>
				</footer>
			</div>
		)
	}
})
