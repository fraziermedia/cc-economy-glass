CustomOrder.Summary = React.createClass({

	componentDidMount: function () {
	},

	getInitialState: function () {
		return {
		}
	},

	render: function () {
		var price = "$" + Number(this.props.price).toFixed(2)
		var details = (this.props.details) ? { __html: this.props.details.replace(/\n/g,"<br />") } : { __html: "Please continue with the ordering wizard for accurate pricing." }
		return (
			<div className="Summary column-medium-3">
				<div className="price">
					<div className="label">Product Price</div>
					<div className="value">{price}</div>
				</div>
				<div className="description" dangerouslySetInnerHTML={details}></div>
			</div>
		)
	}
})
