jQuery(document).ready(function($) {

	// Forward the user to the cart after selection is made
	var cartlink = $(".woocommerce-message .wc-forward").attr("href")
	if (cartlink !== undefined) {
		window.location = cartlink
	}

	$(".mw-custom-order").each(function (index) {

		var $self = $(this)

		console.log("Custom product order builder initialized: " + $self.attr("id"))

		// Add the top-level React object
		ReactDOM.render(React.createElement(CustomOrder, {
			form: $self.parents("form"),
			data: CustomOrderData[$self.attr("id")],
			updateTotal: function (total, formId) {
				// Add advanced calculations for final total
				return total
			}
		}), $self.get(0))

	})

})

var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup

var CustomOrder = React.createClass({displayName: "CustomOrder",

	componentDidMount: function () {
		var self = this
		if (typeof gform == "object") {
			gform.addFilter("gform_product_total", self.props.updateTotal)
		}
		this.updateGravityForm()
	},

	getInitialState: function () {
		return {
			price: 0,
			quantity: 0,
			details: "",
			gravityForm: {
				form: this.props.form,
				price: this.props.form.find('.gf-product-price input'),
				details: this.props.form.find(".gf-product-details textarea"),
			}
		}
	},

	completeOrder: function () {
		this.props.form.find('input[type="submit"]').click()
	},

	updateOrder: function (price, quantity, details) {
		this.setState({
			price: price,
			quantity: quantity,
			details: details
		}, this.updateGravityForm)
	},

	updateGravityForm: function () {
		console.log("Updating Gravity Form")
		var form = this.state.gravityForm
		form.details.val(this.state.details)
		form.price.val(this.state.price).change()
	},

	uploadFile: function (event) {
		event.preventDefault()
		this.props.form.find('input[type="file"]').click()
	},

	render: function () {

		// Set up the child props
		var props = {
			price: this.state.price,
			quantity: this.state.quantity,
			details: this.state.details,
			data: this.props.data,
			updateOrder: this.updateOrder,
			completeOrder: this.completeOrder,
			uploadFile: this.uploadFile
		}

		return (
			React.createElement("div", {className: "CustomOrder mw-row"}, 
				React.createElement(CustomOrder.Summary, React.__spread({},   props)), 
				React.createElement(CustomOrder.Order, React.__spread({},   props))
			)
		)
	}
})

CustomOrder.Order = React.createClass({displayName: "Order",

	componentDidMount: function () {
	},

	getInitialState: function () {
		return {
			minDimension: 1,
			maxDimension: 100,
			width: undefined,
			height: undefined,
			strength: "Annealed",
			thickness: 4,
			tint: undefined,
			textured: 0,
			backpainted: false,
			glass: undefined,
			glassName: undefined,
			edgework: undefined,
			fabrications: [],
			currentStep: 0
		}
	},

	getThicknessLabel: function (thickness) {
		return _.findWhere(this.props.data.thickness, { Thickness: thickness.toString() }).Label
	},

	getGlassOptions: function () {
		console.log("Filtering glass options")
		var self = this
		// Find matching glasses
		var options = _.map(_.filter(self.props.data.glass, function (option) {
			var found = true
			// Is the glass strength available for this brand?
			if (option[self.state.strength] == "0") { found = false }
			// Is this thickness available?
			if (option.Thickness !== self.state.thickness.toString()) { found = false }
			// Does the glass come textured or not?
			if (option.Textured !== self.state.textured.toString()) { found = false }
			return found
		}), function (option) {
			option.Label = self.getThicknessLabel(option.Thickness)
			option.Price = Number(option[self.state.strength]).toFixed(2)
			return option
		})
		return options
	},

	getEdgeworkOptions: function () {
		console.log("Filtering edgework options")
		var self = this
		return _.filter(self.props.data.edgework, function (option) {
			var found = true
			// Is this thickness available?
			if (option.Thickness !== self.state.thickness.toString()) { found = false }
			return found
		})
	},

	getFabricationOptions: function () {
		console.log("Filtering fabrication options")
		var self = this
		return _.filter(self.props.data.fabrication, function (option) {
			var found = true
			// Is this thickness available?
			if (option.Thickness !== self.state.thickness.toString()) { found = false }
			return found
		})
	},

	handleInput: function (event) {
		var self = this
		var regexNumber = /^[\d.]+$/,
			state = {},
			key = event.target.name,
			value = event.target.value

		// Validate data
		switch (key) {
			case "width":
				if (regexNumber.test(value)) {
					value = parseFloat(value)
					if (this.state.strength === "Annealed") {
						if (value < this.state.minDimension) {  value = this.state.minDimension }
						if (value > this.state.glass.MaxWidthA) {
							value = this.state.glass.MaxWidthA
							window.alert("Please select a value equal to or less than " + this.state.glass.MaxWidthA)
						}
					} else {
						if (value < this.state.minDimension) {  value = this.state.minDimension }
						if (value > this.state.glass.MaxWidthT) {
							value = this.state.glass.MaxWidthT
							window.alert("Please select a value equal to or less than " + this.state.glass.MaxWidthT)
						}
					}
				}
				break
			case "height":
				if (regexNumber.test(value)) {
					value = parseFloat(value)
					if (this.state.strength === "Annealed") {
						if (value < this.state.minDimension) {  value = this.state.minDimension }
						if (value > this.state.glass.MaxHeightA) {
							value = this.state.glass.MaxHeightA
							window.alert("Please select a value less than " + this.state.glass.MaxHeightA)
						}
					} else {
						if (value < this.state.minDimension) {  value = this.state.minDimension }
						if (value > this.state.glass.MaxHeightT) {  
							value = this.state.glass.MaxHeightT
							window.alert("Please select a value less than " + this.state.glass.MaxHeightT)
						}
					}
				}
				break
			case "thickness":
				value = parseInt(value)
				state.edgework = undefined
				state.fabrications = []
				break
			case "textured":
				value = (value == "No") ? 0 : 1
				break
			case "strength":
				state.thickness = parseFloat(_.find(self.props.data.glass, function (glass) {
					return (glass.Name == self.state.glassName && glass[value] !== "0")
				}).Thickness)
				break
		}
		state[key] = value
		this.setState(state, self.setGlass)
	},

	setGlass: function () {
		var self = this,
			glass = _.findWhere(self.props.data.glass, {
				Name: self.state.glassName,
				Thickness: self.state.thickness.toString()
			})
		glass.Label = self.getThicknessLabel(glass.Thickness)
		glass.Price = Number(glass[self.state.strength]).toFixed(2)
		if (parseFloat(glass.Price) === 0) {
			// Glass not available
			console.log("Glass not available: " + glass.Name)
			this.setState({
				glass: undefined,
				glassName: undefined,
				edgeword: undefined,
				fabrications: []
			}, self.updateOrder)
		} else {
			console.log("Setting glass to: " + glass.Name)
			this.setState({
				glass: glass
			}, self.updateOrder)
		}
	},

	setGlassName: function (event) {
		var self = this,
			glassName = event.target.value
		this.setState({
			glassName: glassName,
			edgework: undefined,
			thickness: parseFloat(_.find(self.props.data.glass, function (glass) {
				return (glass.Name == glassName && glass[self.state.strength] !== "0")
			}).Thickness)
		}, self.setGlass)
	},

	setEdgework: function (event) {
		event.preventDefault()
		var self = this,
			edgework = JSON.parse(event.target.value)
		this.setState({
			edgework: edgework
		}, self.updateOrder)
	},

	addFabrication: function (fabrication, event) {
		event.preventDefault()
		var self = this,
			fabrications = this.state.fabrications
		fabrications.push(fabrication)
		this.setState({
			fabrications: fabrications
		}, self.updateOrder)
	},

	removeFabrication: function (index, event) {
		event.preventDefault()
		var self = this
			fabrications = this.state.fabrications
		fabrications.splice(index, 1)
		this.setState({
			fabrications: fabrications
		}, self.updateOrder)
	},

	setNextStep: function (event) {
		event.preventDefault()
		var self = this,
			currentStep = self.state.currentStep + 1,
			edgeworkOptions = self.getEdgeworkOptions(),
			fabricationOptions = self.getFabricationOptions()

		// Skip edgework or fabrication if they're not available for glass
		if (currentStep == 2 && edgeworkOptions.length == 1) { currentStep = currentStep + 1 }
		if (currentStep == 3 && fabricationOptions.length === 0) { currentStep = currentStep + 1 }

		if (this.state.currentStep === 0 && this.state.glassName === undefined) {
			alert("Please select a glass type before proceeding.")
			return
		}

		if (this.state.currentStep === 1 && (this.state.width === undefined || this.state.height === undefined)) {
			alert("Please set your width and height before proceeding.")
			return
		}

		// If we're on the 4th step, submit the form
		if (currentStep == 5) { self.props.completeOrder() }

		console.log("Changing to next step: " + currentStep)
		this.setState({
			currentStep: currentStep
		}, self.updateOrder)
	},

	setPreviousStep: function (event) {
		var self = this,
			currentStep = self.state.currentStep - 1,
			edgeworkOptions = self.getEdgeworkOptions(),
			fabricationOptions = self.getFabricationOptions()

		event.preventDefault()

		// Skip edgework or fabrication if they're not available for glass
		if (currentStep == 3 && fabricationOptions.length === 0) { currentStep = currentStep - 1 }
		if (currentStep == 2 && edgeworkOptions.length == 1) { currentStep = currentStep - 1 }

		console.log("Changing to previous step")

		this.setState({
			currentStep: currentStep
		}, self.updateOrder)
	},

	updateOrder: function () {
		var self = this,
			price = 0,
			edgework = 0,
			fabrications = 0,
			total = 0,
			orderDetails = ""

		// Make sure a glass type / brand has been selected!
		if (self.state.glass !== undefined && self.state.width !== undefined && self.state.height !== undefined) {

			var glass = self.state.glass
			var width = isNaN(parseFloat(self.state.width)) ? 0 : parseFloat(self.state.width)
			var height = isNaN(parseFloat(self.state.height)) ? 0 : parseFloat(self.state.height)

			orderDetails = 	glass.Name + "\n" +
								"  Strength: " + self.state.strength + "\n" +
								"  Textured: " + ((glass.Textured == "0") ? "No" : "Yes") + "\n" + 
								"  Thickness: " + glass.Label + "\n" +
								"  Dimensions: " + width + "\" (w) x " + height + "\" (h)\n\n"

			// Get the size in square inches
			var sizeSquareInch = width * height
			if (sizeSquareInch < glass.SqrMinTmp * 144) { sizeSquareInch = parseFloat(glass.SqrMinTmp) * 144 }

			var markup = (self.state.strength == "Annealed") ? glass.MarkupA : glass.MarkupT

			// Get the base price
			price = ((sizeSquareInch / 144) * glass.Price) * markup

			// Add edgework pricing
			if (self.state.edgework !== undefined) {
				edgework = ((width + height) * 2) * parseFloat(self.state.edgework.PricePerInch)
				orderDetails +=	"Edgework: " + self.state.edgework.Name + "\n\n"
			}

			// Add fabrication pricing
			if (self.state.fabrications.length > 0) {
				orderDetails +=	"Fabrications: \n"
				fabrications = _.reduce(self.state.fabrications, function (memo, fabrication) {
					memo = memo + Number(fabrication.Price)
					orderDetails += "- " + fabrication.Name + "\n"
					return memo
				}, 0)
			}

		} else {

			orderDetails = "Please continue with the ordering wizard for accurate pricing."

		}

		// Add it up... add it up...
		total = price + edgework + fabrications

		console.log("Order total: " + total)
		console.log(orderDetails)
		this.props.updateOrder( total, 1, orderDetails )
	},

	render: function () {
		var self = this

		var availableGlass = _.filter(self.props.data.glass, function (glass) {
			return parseFloat(glass[self.state.strength]) > 0
		})

		var glassOptions = _.uniq(_.pluck(availableGlass, "Name"))

		var thicknessOptions = _.map(_.pluck(_.where(availableGlass, { Name: self.state.glassName }), "Thickness"), function (thickness, index) {
			var thicknessLabel = _.findWhere(self.props.data.thickness, { Thickness: thickness })
			return (
				React.createElement("option", {key: "Thickness"+index, value: thicknessLabel.Thickness}, thicknessLabel.Label)
			)
		})

		var textured = (this.state.textured === 0) ? false : true
		var edgeworkOptions = self.getEdgeworkOptions()
		var fabricationOptions = self.getFabricationOptions()

		var fabrications
		if (this.state.fabrications.length === 0) {
			fabrications = (
				React.createElement("div", {className: "instructions"}, "None selected")
			)
		} else {
			fabrications = (
				React.createElement("div", {className: "selection-list"}, 
					_.map(self.state.fabrications, function (fabrication, index) {
						return (
							React.createElement("div", {key: "Fabrication" + index, className: "fabrication-selection mw-row option"}, 
								React.createElement("div", {className: "label column-medium-6"}, fabrication.Name), 
								React.createElement("div", {className: "control column-medium-6"}, React.createElement("button", {className: "fabrication-button", onClick: self.removeFabrication.bind(null, index)}, "Remove"))
							)
						)
					})
				)
			)
		}

		var steps = [
					React.createElement(CustomOrder.Step, {
						key: "StepMaterial", 
						className: "Material", 
						label: "Select Glass Material", 
						next: React.createElement("button", {className: "primary button", onClick: this.setNextStep}, "Next Step")}, 
						React.createElement("table", null, 
							React.createElement("tbody", null, 
								React.createElement("tr", null, 
									React.createElement("th", null, "Strength"), 
									React.createElement("td", {className: "radio-group"}, 
										_.map(["Annealed", "Tempered"], function (option) {
											var checked = (self.state.strength == option)
											return (
												React.createElement("label", {key: "Strength" + option}, React.createElement("input", {name: "strength", type: "radio", value: option, checked: checked, onChange: self.handleInput}), " ", option)
											)
										})
									)
								), 
								React.createElement("tr", null, 
									React.createElement("th", null, "Available Glass"), 
									React.createElement("td", null, 
										React.createElement("select", {size: "8", className: "glass-options selection-list", value: self.state.glassName, onChange: self.setGlassName}, 
											_.map(glassOptions, function (glassName, index) {
												return (
													React.createElement("option", {key: "Glass" + index, value: glassName}, 
														glassName
													)
												)
											})
										)
									)
								)
							)
						)
					),

					React.createElement(CustomOrder.Step, {
						key: "StepDimensions", 
						className: "Dimensions", 
						label: "Select Pane Dimensions", 
						previous: React.createElement("button", {className: "secondary button", onClick: this.setPreviousStep}, "Previous"), 
						next: React.createElement("button", {className: "primary button", onClick: this.setNextStep}, "Next Step")}, 
						React.createElement("table", null, 
							React.createElement("tbody", null, 
								React.createElement("tr", null, 
									React.createElement("th", null, "Width (inches)"), 
									React.createElement("td", null, React.createElement("input", {name: "width", placeholder: "Width in inches", type: "number", step: "0.1", value: this.state.width, onChange: this.handleInput}))
								), 
								React.createElement("tr", null, 
									React.createElement("th", null, "Height (inches)"), 
									React.createElement("td", null, React.createElement("input", {name: "height", placeholder: "Height in inches", type: "number", step: "0.1", value: this.state.height, onChange: this.handleInput}))
								), 
								React.createElement("tr", null, 
									React.createElement("th", null, "Thickness"), 
									React.createElement("td", null, 
										React.createElement("select", {name: "thickness", value: this.state.thickness.toString(), onChange: this.handleInput}, 
											thicknessOptions
										)
									)
								)
							)
						)
					),

					React.createElement(CustomOrder.Step, {
						key: "StepEdgework", 
						className: "Edgework", 
						label: "Select Edgework", 
						previous: React.createElement("button", {className: "secondary button", onClick: this.setPreviousStep}, "Previous"), 
						next: React.createElement("button", {className: "primary button", onClick: this.setNextStep}, "Next Step")}, 
						React.createElement("select", {size: "8", className: "edgework-options selection-list", value: JSON.stringify(self.state.edgework), onChange: self.setEdgework}, 
							_.map(edgeworkOptions, function (option, index) {
								return (
									React.createElement("option", {key: "Edgework" + index, value: JSON.stringify(option)}, 
										option.Name
									)
								)
							})
						)
					),

					React.createElement(CustomOrder.Step, {
						key: "StepFabrication", 
						className: "Fabrication", 
						label: "Select Fabrication Options", 
						previous: React.createElement("button", {className: "secondary button", onClick: this.setPreviousStep}, "Previous"), 
						next: React.createElement("button", {className: "primary button", onClick: this.setNextStep}, "Next Step")}, 

						React.createElement("div", {className: "mw-row"}, 
							React.createElement("div", {className: "instructions"}, "All fabrications are measured to the center of the hole, not the edge."), 
							React.createElement("div", {className: "fabrication-options column-medium-6"}, 
								React.createElement("h3", null, "Available Fabrication Options"), 
								React.createElement("div", {className: "selection-list"}, 
									_.map(fabricationOptions, function (option, index) {
										return (
											React.createElement("div", {key: "FabricationOption" + index, className: "fabrication-option option", onClick: self.addFabrication.bind(null, option)}, option.Name)
										)
									})
								)
							), 
							React.createElement("div", {className: "fabrication-selections column-medium-6"}, 
								React.createElement("h3", null, "Current Fabrication Selections"), 
								fabrications
							)
						)
					),

					React.createElement(CustomOrder.Step, {
						key: "StepUpload", 
						className: "Upload", 
						label: "Upload Special Instructions", 
						previous: React.createElement("button", {className: "secondary button", onClick: this.setPreviousStep}, "Previous"), 
						next: React.createElement("button", {className: "primary button", onClick: this.props.completeOrder}, "Complete Order")}, 

						React.createElement("p", null, "If you have a drawing of your glass design or would like to provide additional instructions, please upload those materials here."), 
						React.createElement("div", {className: "upload-row"}, 
							React.createElement("button", {className: "button upload", onClick: self.props.uploadFile}, "Select Uploads")
						)

					)
		]

		return (
			React.createElement("div", {className: "Order column-medium-9"}, 
				React.createElement(ReactCSSTransitionGroup, {transitionName: "step", className: "Steps", transitionEnterTimeout: 500, transitionLeaveTimeout: 500}, 
					steps[this.state.currentStep]
				)
			)
		)
	}
})

CustomOrder.Step = React.createClass({displayName: "Step",

	componentDidMount: function () {
	},

	getInitialState: function () {
		return {
		}
	},

	render: function () {
		var self = this

		var classes = [
			"Step",
			self.props.className
		].join(" ")

		return (
			React.createElement("div", {className: classes}, 
				React.createElement("header", null, 
					React.createElement("h2", null, this.props.label), 
					React.createElement("p", null, this.props.instructions)
				), 
				React.createElement("main", null, this.props.children), 
				React.createElement("footer", {className: "controls mw-row"}, 
					React.createElement("div", {className: "previous column-medium-6 column-small-6"}, this.props.previous, " "), 
					React.createElement("div", {className: "next column-medium-6 column-small-6"}, " ", this.props.next)
				)
			)
		)
	}
})

CustomOrder.Summary = React.createClass({displayName: "Summary",

	componentDidMount: function () {
	},

	getInitialState: function () {
		return {
		}
	},

	render: function () {
		var price = "$" + Number(this.props.price).toFixed(2)
		var details = (this.props.details) ? { __html: this.props.details.replace(/\n/g,"<br />") } : { __html: "Please continue with the ordering wizard for accurate pricing." }
		return (
			React.createElement("div", {className: "Summary column-medium-3"}, 
				React.createElement("div", {className: "price"}, 
					React.createElement("div", {className: "label"}, "Product Price"), 
					React.createElement("div", {className: "value"}, price)
				), 
				React.createElement("div", {className: "description", dangerouslySetInnerHTML: details})
			)
		)
	}
})
